<!DOCTYPE html>
<html>
  <head>
    <h1 id="title">Buat Account Baru!</h1>
  </head>
  <body>
  <form id="survey-form" method="POST" action="welcome">
    @csrf
    <h3>Sign Up Form</h3>
    <label id="first-name-label" for="first-name">First name:</label><br><br>
        <input id="first-name" name="first-name" type="text"><br><br>
    <label id="last-name-label" for="last-name">Last name:</label><br><br>
        <input id="last-name" name="last-name" type="text"><br><br>
    <label id="gender-label" for="gender">Gender:</label><br><br>
        <label id="radio"><input value="Male" type="radio" id="gender">Male 
        </label><br>
        <label id="radio"><input value="Female" type="radio" id="gender">Female 
        </label><br>
        <label id="radio"><input value="Other" type="radio" id="gender">Other 
        </label><br><br>
    <label id="nationality-label" for="nationality">Nationality:</label><br><br>
      <select id="dropdown">
          <option value="Indonesia" id="nationality">Indonesia</option>
          <option value="Other" id="nationality">Other</option>
      </select><br><br>
    <label id="language-label" for="language">Language spoken:</label><br><br>
          <label id="checkbox"><input value="Bahasa Indonesia" type="checkbox" id="language">Bahasa Indonesia</label><br>
          <label id="checkbox"><input value="English" type="checkbox" id="language">English</label><br>
          <label id="checkbox"><input value="Other" type="checkbox" id="language">Other</label><br><br>
    <label id="bio-label" for="bio">Bio:</label><br><br>
        <textarea
        id="bio">
        </textarea><br><br>
    <button id="sign-up" type="submit">Sign Up</button>  
  </form>
  </body>
</html>