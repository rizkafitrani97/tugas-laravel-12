<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
    return view('form');
}
    public function welcome(Request $request) {
    $namaDepan = $request["first-name"];
    $namaBelakang = $request["last-name"];
    return "<h1>Selamat Datang $namaDepan $namaBelakang !</h1>".view('welcome');		
}
}
